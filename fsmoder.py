#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import getopt
import sys
import os
import errno
import json
import stat
import fnmatch
import logging
_log = logging.getLogger(__name__)

_HELP_TEXT = """
Arguments: [Action] [Options...]

Actions:

    --generate | -g
        Generate file system object status record.

    --validate | -V
        Validate (Dry-run) file system from given status record.

    --modify
        Modify file system from given status record.

Options:

    --help | -h
        Prints help message.

    --record-folder=[RECORD_FOLDER_PATH]
    -f [RECORD_FOLDER_PATH]
        Specify path of record folder. (required)

    --ignore-pattern=[IGNORE_NAME_PATTERN]
    -i [IGNORE_NAME_PATTERN]
        File or folder name pattern to be ignored.

    --skip=[PATH_TO_SKIP]
    -s [PATH_TO_SKIP]
        Path of file or folder to skip.
"""

ACTION_GENERATE = 1
ACTION_VALIDATE = 2
ACTION_MODIFY = 3


def _do_exit(exit_code, *msg_args):
	if msg_args:
		_log.critical(*msg_args)
	sys.exit(exit_code)
	return None


def _collect_folders(folders, base_folder, as_set=False):
	ff = set()
	for p in folders:
		if len(p) < 1:
			continue
		if p[0] != "/":
			p = os.path.join(base_folder, p)
		p = os.path.abspath(os.path.realpath(p))
		ff.add(p)
	if as_set:
		return ff
	if ff:
		return sorted(ff)
	return None


def parse_options(argv):  # noqa: MC0001
	# pylint:disable=too-many-locals
	ACTION_CODEMAP = {
			"-g": ACTION_GENERATE,
			"--generate": ACTION_GENERATE,
			"-V": ACTION_VALIDATE,
			"--validate": ACTION_VALIDATE,
			"--modify": ACTION_MODIFY,
	}
	log_verbose_mode = logging.WARNING
	action = 0
	record_folder = None
	ignore_patterns = set()
	skip_paths = set()
	try:
		opts, args, = getopt.getopt(argv, "hvgVf:i:s:", (
				"help",
				"generate",
				"validate",
				"modify",
				"record-folder=",
				"ignore-pattern=",
				"skip=",
		))
	except getopt.GetoptError as e:
		print str(e)
		print _HELP_TEXT
		return _do_exit(1)
	for opt, arg, in opts:
		if opt in ("-h", "--help"):
			print _HELP_TEXT
			return _do_exit(2)
		elif opt == "-v":
			_VERBOSE_ADVANCE = {
					logging.WARNING: logging.INFO,
					logging.INFO: logging.DEBUG,
			}
			log_verbose_mode = _VERBOSE_ADVANCE.get(log_verbose_mode, log_verbose_mode)
		elif opt in ACTION_CODEMAP:
			action = ACTION_CODEMAP[opt]
		elif opt in ("-f", "--record-folder"):
			record_folder = arg
		elif opt in ("-i", "--ignore-pattern"):
			ignore_patterns.add(arg)
		elif opt in ("-s", "--skip"):
			skip_paths.add(arg)
		else:
			_log.error("unrecongize option: %r", opt)
	base_folder = os.getcwd()
	record_folder = os.path.abspath(record_folder)
	if not os.path.isdir(record_folder):
		os.makedirs(record_folder)
	target_folders = _collect_folders(args, base_folder)
	if not target_folders:
		return _do_exit(1, "target folder is required")
	if not record_folder:
		return _do_exit(1, "record folder is required")
	ignore_patterns = tuple(ignore_patterns)
	skip_paths.add(record_folder)
	skip_paths = _collect_folders(skip_paths, base_folder, as_set=True)
	return (
			log_verbose_mode,
			action,
			record_folder,
			base_folder,
			target_folders,
			ignore_patterns,
			skip_paths,
	)


def encode_path(p):
	if p == ".":
		p = ""
	elif (len(p) > 2) and (p[0:2] == "./"):
		p = p[2:]
	r = p.replace("/", "_").replace(".", "_")
	if not r:
		return "_fs_root_"
	return r


def is_match_fnrules(n, rules):
	for r in rules:
		if fnmatch.fnmatch(n, r):
			return True
	return False


_MODE_BIT_MASK = stat.S_ISUID | stat.S_ISGID | stat.S_ISVTX | stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO | stat.S_ENFMT


def write_stat_as_json(fp, name, st):
	aux = {
			"name": name,
			"uid": st.st_uid,
			"gid": st.st_gid,
			"mode": oct(st.st_mode & _MODE_BIT_MASK),
	}
	t = json.dumps(aux, sort_keys=True)
	fp.write(t)
	fp.write("\n")


def generate_record(record_folder, base_folder, target_folders, ignore_patterns, skip_paths):
	for full_path in target_folders:
		for root, dirs, files, in os.walk(full_path):
			rec_name = encode_path(os.path.relpath(root, base_folder))
			_log.info("save record to: %r, %r", record_folder, rec_name)
			with open(os.path.join(record_folder, rec_name), "w") as fp:
				for n in files:
					if is_match_fnrules(n, ignore_patterns):
						_log.warning("skip file: %r, %r", root, n)
						continue
					p = os.path.join(root, n)
					try:
						write_stat_as_json(fp, n, os.lstat(p))
					except Exception:
						_log.exception("skip: file cannot lstat and record: %r, %r", root, n)
				to_skip = []
				for n in dirs:
					p = os.path.join(root, n)
					if (p in skip_paths) or is_match_fnrules(n, ignore_patterns):
						_log.warning("skip folder: %r, %r", root, n)
						to_skip.append(n)
						continue
					try:
						write_stat_as_json(fp, n, os.lstat(p))
					except Exception:
						_log.exception("skip: folder cannot lstat and record: %r, %r", root, n)
				for n in to_skip:
					dirs.remove(n)


def get_stat_from_json(l):
	cmap = json.loads(l.strip())
	uid = int(cmap["uid"])
	gid = int(cmap["gid"])
	mode = int(cmap["mode"], 8)
	n = cmap["name"]
	return (
			n,
			uid,
			gid,
			mode,
	)


def load_record_file(record_folder, rec_name):
	with open(os.path.join(record_folder, rec_name), "r") as fp:
		result = {}
		for l in fp:
			try:
				n, uid, gid, mode, = get_stat_from_json(l)
				result[n] = (
						uid,
						gid,
						mode,
				)
			except Exception:
				_log.error("cannot having record from record line: %r", l)
	return result


def adjust_fsmode_by_record(root, n, rec, ignore_patterns, skip_paths, dry_run=False):
	# pylint:disable=too-many-arguments
	if n not in rec:
		_log.warning("skip: record not found: %r, %r", root, n)
		return False
	if is_match_fnrules(n, ignore_patterns):
		_log.warning("skip: by ignore rule: %r, %r", root, n)
		return False
	p = os.path.join(root, n)
	if p in skip_paths:
		_log.warning("skip: by skip paths: %r, %r", root, n)
		return False
	uid, gid, mode, = rec[n]
	try:
		st = os.lstat(p)
		if (st.st_uid != uid) or (st.st_gid != gid):
			if not dry_run:
				_log.info("adjust owner: %r, %r -> %r, %r", root, n, uid, gid)
				os.chown(p, uid, gid)
			else:
				_log.info("dry-run: chown(%r, %r, %r)", p, uid, gid)
		if (st.st_mode & _MODE_BIT_MASK) != mode:
			if not dry_run:
				_log.info("adjust mode: %r, %r -> %r", root, n, oct(mode))
				os.chmod(p, mode)
			else:
				_log.info("dry-run: chmod(%r, %s, from=%s)", p, oct(mode), oct(st.st_mode))
	except Exception:
		_log.warning("failed on adjusting owner of mode: %r, %r", root, n)
	return True


def modify_fsmode(record_folder, base_folder, target_folders, ignore_patterns, skip_paths, dry_run):
	# pylint:disable=too-many-arguments
	for full_path in target_folders:
		for root, dirs, files, in os.walk(full_path):
			rec_name = encode_path(os.path.relpath(root, base_folder))
			_log.debug("load record from: %r, %r", record_folder, rec_name)
			try:
				rec = load_record_file(record_folder, rec_name)
			except IOError as e:
				if e.errno == errno.ENOENT:
					_log.warning("skip: cannot reach record file: %r, %r", record_folder, rec_name)
				else:
					_log.exception("caught IO error on load record from file: %r, %r", record_folder, rec_name)
				continue
			except Exception:
				_log.exception("cannot load record from file: %r, %r", record_folder, rec_name)
				continue
			for n in files:
				adjust_fsmode_by_record(root, n, rec, ignore_patterns, skip_paths, dry_run)
			to_skip = []
			for n in dirs:
				if not adjust_fsmode_by_record(root, n, rec, ignore_patterns, skip_paths, dry_run):
					to_skip.append(n)
			for n in to_skip:
				dirs.remove(n)



def main():
	log_verbose_mode, action, record_folder, base_folder, target_folders, ignore_patterns, skip_paths, = parse_options(sys.argv[1:])
	logging.basicConfig(stream=sys.stderr, level=log_verbose_mode)

	if action == ACTION_GENERATE:
		generate_record(record_folder, base_folder, target_folders, ignore_patterns, skip_paths)
	elif action == ACTION_VALIDATE:
		modify_fsmode(record_folder, base_folder, target_folders, ignore_patterns, skip_paths, True)
	elif action == ACTION_MODIFY:
		modify_fsmode(record_folder, base_folder, target_folders, ignore_patterns, skip_paths, False)
	else:
		_log.critical("unknown action code: %r", action)


if __name__ == "__main__":
	sys.exit(main())
